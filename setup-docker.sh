#!/usr/bin/env bash
set -eu

if ! $(zfs list zroot/var/lib/docker > /dev/null 2>&1); then
    zfs create -o com.sun:auto-snapshot=false zroot/var/lib/docker
    pacman --sync --noconfirm \
    docker \
    docker-compose
    systemctl enable docker.service
fi
