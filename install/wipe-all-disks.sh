#!/usr/bin/env bash

# 8 (disks), 259 (nvme)

function disk_devices() {
    lsblk --path --nodeps --noheadings --output NAME --include 259 | sort
}

lsblk --path --nodeps --noheadings --output NAME --include 259 \
| xargs --no-run-if-empty wipefs --all --force

for device in $(disk_devices); do
    sgdisk --zap-all $device
    sgdisk --randomize-guids $device
done
