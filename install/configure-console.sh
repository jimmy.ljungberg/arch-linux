#!/usr/bin/env bash
set -eu

#FONT="ter-powerline-v22b"
FONT_MAP="8859-1"

#sudo setfont $FONT -m $FONT_MAP

echo "KEYMAP=sv-latin1" > /etc/vconsole.conf
#echo "FONT=$FONT" >> /etc/vconsole.conf
echo "FONT_MAP=$FONT_MAP" >> /etc/vconsole.conf
