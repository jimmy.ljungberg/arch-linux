#!/usr/bin/env bash
set -eux

PACKAGE=vim-colors-solarized-git
rm -fr $PACKAGE
sudo -u $USER_NAME git clone https://aur.archlinux.org/$PACKAGE.git
cd $PACKAGE

# "Hack" för att fixa installation
sed -i 's/:git:/:git\+https:/' PKGBUILD

sudo -u $USER_NAME makepkg
pacman -U --noconfirm $(ls -1 $PACKAGE*.zst)
