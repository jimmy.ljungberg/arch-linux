#!/usr/bin/env bash
set -eu

pacman -S --noconfirm \
apparmor \
go \
go-tools \
python-docutils \
squashfs-tools \
xfsprogs

PACKAGE=snapd
rm -fr $PACKAGE
sudo -u $USER_NAME git clone https://aur.archlinux.org/$PACKAGE.git
cd $PACKAGE

sudo -u $USER_NAME makepkg
pacman -U --noconfirm $(ls -1 $PACKAGE*.zst)

systemctl enable --now snapd.socket
systemctl enable --now snapd.apparmor
systemctl enable --now apparmor.service
systemctl enable --now snapd.apparmor.service
