#!/usr/bin/env bash
set -eu

sed -i 's/^HOOKS.*$/HOOKS=(base udev autodetect modconf kms keyboard keymap consolefont block lvm2 filesystems fsck)/g' /etc/mkinitcpio.conf
mkinitcpio -p linux
