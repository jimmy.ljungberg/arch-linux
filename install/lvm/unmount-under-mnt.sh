#!/usr/bin/env bash
set -eu

findmnt --noheadings --list --real --output TARGET | grep /mnt | sort --reverse | xargs --no-run-if-empty umount
