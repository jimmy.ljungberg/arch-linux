#!/usr/bin/env bash
set -eu

pacstrap -K /mnt \
amd-ucode \
base \
linux \
linux-firmware \
lvm2 \
networkmanager \
vim
