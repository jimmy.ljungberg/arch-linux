#!/usr/bin/env bash
set -eu

umount /boot
mount /boot
bootctl install

cat <<-EOF > /boot/loader/loader.conf
default arch
timeout 4
editor  0
EOF

cat <<-EOF > /boot/loader/entries/arch-${VOLUME_GROUP}.conf
title   Arch Linux
linux   /vmlinuz-linux
initrd  /initramfs-linux.img
options root=/dev/mapper/${VOLUME_GROUP}-root rw
EOF
ls