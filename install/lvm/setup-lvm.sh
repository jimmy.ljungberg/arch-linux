#!/usr/bin/env bash
set -eu

# MAJOR = 8, disks, MAJOR = 259 nvme
MAJOR=259

function identify_disks() {
    lsblk --path --nodeps --noheadings --output NAME --include $MAJOR | sort
}

function get_all_partitions() {
    lsblk --path --include $MAJOR --noheading --pairs | grep part | sort | cut -d\" -f2
}

# Behöver avgöra om disk eller nvme (behöver lägga till 'p' för nvme, /dev/nvme0n1p1)
function identify_boot_partition() {
    echo ${BOOT_DEVICE}p$(parted --script --machine $BOOT_DEVICE print | grep boot | cut -d: -f1)
}

function create_physical_volumes() {
    for device in $(get_all_partitions); do
        if [ "$device" != "$(identify_boot_partition)" ]; then
            pvcreate --force $device
        fi
    done
}

function create_volume_group() {
    vgcreate --yes "$VOLUME_GROUP" $(echo -n $(pvs --noheadings --options pv_name))
}

function create_logical_volumes() {
    lvcreate --yes -L 20G "$VOLUME_GROUP" -n root
    lvcreate --yes -L 8G "$VOLUME_GROUP" -n swap
    lvcreate --yes -L 10G "$VOLUME_GROUP" -n home
}

create_physical_volumes
create_volume_group
create_logical_volumes
