#!/usr/bin/env bash
set -eu

genfstab -U /mnt \
| sed 's/fmask=0022/fmask=0137/g' \
| sed 's/dmask=0022/dmask=0027/g' >> /mnt/etc/fstab
