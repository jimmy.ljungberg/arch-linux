#!/usr/bin/env bash
set -eu

swapoff -a
lvs --noheadings --options lv_path | xargs --no-run-if-empty lvremove --yes --force 1> /dev/null
vgs --noheadings --options vg_name | xargs --no-run-if-empty vgremove --yes --force 1> /dev/null
pvs --noheadings --options pv_name | xargs --no-run-if-empty pvremove --yes --force 1> /dev/null
