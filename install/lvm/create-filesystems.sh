#!/usr/bin/env bash
set -eu

function boot_partition() {
    echo ${BOOT_DEVICE}p$(parted --script --machine $BOOT_DEVICE print | grep boot | cut -d: -f1)
}

mkfs.fat -F32 $(boot_partition)

mkfs.ext4 /dev/${VOLUME_GROUP}/root 1> /dev/null
mkfs.ext4 /dev/${VOLUME_GROUP}/home 1> /dev/null

mkswap /dev/${VOLUME_GROUP}/swap 1> /dev/null
swapon /dev/${VOLUME_GROUP}/swap 1> /dev/null
