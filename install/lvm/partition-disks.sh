#!/usr/bin/env bash
set -eu

# MAJOR=8, disks, MAJOR=259 nvme
MAJOR=259

function identify_disks() {
    lsblk --path --nodeps --noheadings --output NAME --include $MAJOR | sort
}

function prepare_boot_device() {
    parted --script --align optimal $1 mklabel gpt \
    mkpart primary fat32 1MiB 1GiB set 1 esp on \
    mkpart primary ext4 1GiB 100%
}

function prepare_device() {
    parted --script --align optimal $1 mklabel gpt \
    mkpart primary ext4 1MiB 100%
}

for device in $(identify_disks); do
    wipefs --all --force $device 1> /dev/null
    if [ "$device" == "$BOOT_DEVICE" ]; then
        prepare_boot_device $device
    else
        prepare_device $device
    fi
done
