#!/usr/bin/env bash
set -eu

function boot_partition() {
    echo ${BOOT_DEVICE}p$(parted --script --machine $BOOT_DEVICE print | grep boot | cut -d: -f1)
}

mount --mkdir /dev/${VOLUME_GROUP}/root /mnt
mount --mkdir $(boot_partition) /mnt/boot
mount --mkdir /dev/${VOLUME_GROUP}/home /mnt/home
