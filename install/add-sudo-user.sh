#!/usr/bin/env bash
set -eu

useradd -m -G wheel -s /bin/bash $USER_NAME

rm -f /etc/sudoers.d/$USER_NAME
cat <<-EOF >> /etc/sudoers.d/$USER_NAME
$USER_NAME   ALL=(ALL)   NOPASSWD:ALL
EOF

chmod 400 /etc/sudoers.d/$USER_NAME
