#!/usr/bin/env bash
set -eu

pacman -S --noconfirm \
base-devel \
bash-completion \
git \
net-tools \
networkmanager \
nvidia-dkms \
openssh \
pacman-contrib \
powerline \
python-pip \
reflector \
rsync \
tmux \
tree \
unzip \
wget \
xdg-user-dirs \
xdg-utils \
zip
