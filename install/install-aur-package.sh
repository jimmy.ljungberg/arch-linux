#!/usr/bin/env bash
set -eu

rm -fr $PACKAGE
sudo -u $USER_NAME git clone https://aur.archlinux.org/$PACKAGE.git
cd $PACKAGE
sudo -u $USER_NAME makepkg --syncdeps
pacman -U --noconfirm $(ls -1 $PACKAGE*.zst)
