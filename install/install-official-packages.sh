#!/usr/bin/env bash
set -eu

pacman -S --noconfirm \
alsa-utils \
base-devel \
bash-completion \
bashtop \
bluez \
bluez-utils \
dmidecode \
docker \
docker-compose \
firefox \
git \
gnome \
gnome-shell-extension-appindicator \
gnome-tweaks \
gptfdisk \
libqtxdg \
libxss \
lshw \
lsof \
net-tools \
networkmanager \
nvidia-dkms \
openssh \
pacman-contrib \
powerline \
python-pip \
reflector \
rsync \
solaar \
swtpm \
tmux \
tree \
unzip \
virt-manager \
wget \
xdg-user-dirs \
xdg-utils \
xorg-mkfontscale \
zip \
webp-pixbuf-loader

yes | sudo pacman -S iptables-nft
