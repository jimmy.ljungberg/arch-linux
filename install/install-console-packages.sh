#!/usr/bin/env bash
set -eu

pacman --sync --noconfirm \
base-devel \
bash-completion \
bat \
dkms \
eza \
fd \
fzf \
git \
hwdetect \
jq \
man-db \
man-pages \
neovim \
openssh \
pacman-contrib \
reflector \
rsync \
sudo \
texinfo \
tmux \
tree \
unzip \
wget \
xdg-user-dirs \
xdg-utils \
zip

systemctl enable reflector.timer
