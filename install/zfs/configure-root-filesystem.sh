#!/usr/bin/env bash
set -eu

zpool set cachefile=/etc/zfs/zpool.cache zroot
mkdir -p /mnt/{etc/zfs,boot/efi}
cp /etc/zfs/zpool.cache /mnt/etc/zfs/zpool.cache

mount $DISK_BY_ID-part1 /mnt/boot/efi
