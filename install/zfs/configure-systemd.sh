#!/usr/bin/env bash
set -eu

systemctl enable zfs.target
systemctl enable zfs-import-cache.service
systemctl enable zfs-mount.service
systemctl enable zfs-import.target
systemctl enable zfs-import-scan
systemctl enable zfs-share
systemctl enable zfs-zed
systemctl enable zfs-scrub-weekly@pool-to-scrub.timer

rm -f /etc/hostid
zgenhostid $(hostid)

systemctl enable NetworkManager
systemctl enable reflector.timer
#systemctl enable sshd
#systemctl enable docker.service
#systemctl enable bluetooth.service
#systemctl enable gdm.service
