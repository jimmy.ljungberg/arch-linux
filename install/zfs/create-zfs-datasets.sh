#!/usr/bin/env bash
set -eu

zfs create -o mountpoint=none $POOL_NAME/data
zfs create -o mountpoint=none $POOL_NAME/ROOT
zfs create -o mountpoint=/ -o canmount=noauto $POOL_NAME/ROOT/default
zfs create -o mountpoint=/home $POOL_NAME/data/home

zfs create -o mountpoint=/var -o canmount=off $POOL_NAME/var
zfs create $POOL_NAME/var/log
zfs create -o mountpoint=/var/lib -o canmount=off $POOL_NAME/var/lib
#zfs create $POOL_NAME/var/lib/libvirt
#zfs create -o com.sun:auto-snapshot=false $POOL_NAME/var/lib/docker
