#!/usr/bin/env bash
set -eu

pacstrap /mnt \
amd-ucode \
base \
dkms \
efibootmgr \
grub \
linux \
linux-firmware \
linux-headers \
networkmanager \
vim \
zfs-dkms \
zfs-utils
