#!/usr/bin/env bash
set -eu

cat <<-'EOF' >> /mnt/etc/pacman.conf

[archzfs]
SigLevel = Optional TrustAll
Server = http://archzfs.com/$repo/x86_64
EOF
