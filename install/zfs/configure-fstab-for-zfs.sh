#!/usr/bin/env bash
set -eu

genfstab -U -p /mnt >> /mnt/etc/fstab
sed -i 's/^\(zroot.*\)/#\1/g' /mnt/etc/fstab
