#!/usr/bin/env bash
set -eu

zpool create -f -o ashift=12 \
    -O acltype=posixacl \
    -O relatime=on \
    -O xattr=sa \
    -O dnodesize=legacy \
    -O normalization=formD \
    -O mountpoint=none \
    -O canmount=off \
    -O devices=off \
    -R /mnt \
    -O compression=lz4 \
    $POOL_NAME ${DISK_BY_ID}-part2 
