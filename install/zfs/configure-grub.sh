#!/usr/bin/env bash
set -eu

sed -i 's/^\(GRUB_CMDLINE_LINUX_DEFAULT=\).*/\1\"loglevel=3 nvidia_drm.modeset=1 iommu=pt\"/' /etc/default/grub
sed -i 's/^\(GRUB_CMDLINE_LINUX=\).*/\1\"root=ZFS=zroot\/ROOT\/default\"/' /etc/default/grub
sed -i 's/^\(GRUB_GFXMODE=\).*/\1\"3440x1440x32,auto"/' /etc/default/grub

ZPOOL_VDEV_NAME_PATH=1 grub-install --target=x86_64-efi --efi-directory=/boot/efi --bootloader-id=ArchLinux
ZPOOL_VDEV_NAME_PATH=1 grub-mkconfig -o /boot/grub/grub.cfg
