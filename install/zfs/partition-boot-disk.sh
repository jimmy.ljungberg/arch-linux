#!/usr/bin/env bash
set -eu

sgdisk -n 1:0:+600MiB -t 1:ef00 -c 1:"EFI System Partition" $DISK
sgdisk -n 2:0:$(sgdisk -E $DISK) -t 2:bf00 -c 2:"Solaris root" $DISK
