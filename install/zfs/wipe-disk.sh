#!/usr/bin/env bash
set -eu

umount --force $DISK || /bin/true
umount --force $DISK_BY_ID-part1 || /bin/true
zpool destroy -f $POOL_NAME || /bin/true

sgdisk --zap-all $DISK
sgdisk --randomize-guids $DISK
