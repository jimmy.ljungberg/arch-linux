#!/usr/bin/env bash
set -eux

curl -s $URL/configuration/etc/bash.bashrc > /etc/bash.bashrc
curl -s $URL/configuration/etc/tmux.conf > /etc/tmux.conf
curl -s $URL/configuration/etc/inputrc > /etc/inputrc
curl -s $URL/configuration/etc/vimrc > /etc/vimrc

mkdir -p /etc/pacman.d/hooks
curl -s $URL/configuration/etc/pacman.d/hooks/nvidia.hook > /etc/pacman.d/hooks/nvidia.hook

echo "export EDITOR=vim" >> /etc/profile
echo "export ZPOOL_VDEV_NAME_PATH=1" >> /etc/profile
