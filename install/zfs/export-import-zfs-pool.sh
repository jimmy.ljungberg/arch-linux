#!/usr/bin/env bash
set -eu

zpool export $POOL_NAME
zpool import -d ${DISK_BY_ID}-part2 -R /mnt $POOL_NAME -N

zfs mount zroot/ROOT/default
zfs mount -a
