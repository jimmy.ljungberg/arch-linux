#!/usr/bin/env bash
set -eu

sgdisk -n 1:0:$(sgdisk -E $DISK) -t 1:bf00 -c 1:"Solaris root" $DISK
