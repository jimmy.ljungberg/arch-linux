#!/usr/bin/env bash
set -eu

sed -i 's/^HOOKS.*$/HOOKS=(base udev autodetect modconf block keyboard keymap zfs filesystems)/g' /etc/mkinitcpio.conf
mkinitcpio -p linux

