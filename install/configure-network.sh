#!/usr/bin/env bash
set -eu

echo "$HOST_NAME" > /etc/hostname

rm -f /etc/hosts
cat <<-EOF > /etc/hosts
127.0.0.1   localhost
::1         localhost
127.0.1.1   $HOST_NAME.lan $HOST_NAME
EOF

systemctl enable NetworkManager
