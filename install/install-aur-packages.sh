#!/usr/bin/env bash
set -eu

execute_script () {
    curl -s $URL/install/$1 | bash
}

install_aur () {
    cd /tmp
    curl -s $URL/install/install-aur-package.sh | PACKAGE=$1 bash
}

install_aur enpass-bin
install_aur powerline-fonts-git
install_aur tmux-bash-completion

execute_script install-aur-snapd.sh
execute_script install-aur-vim-color-solarized.sh
