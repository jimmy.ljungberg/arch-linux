#!/usr/bin/env bash
set -eu

execute_script () {
    curl -s $URL/install/$1 | bash
}

execute_script zfs/wipe-disk.sh
execute_script zfs/partition-boot-disk.sh
execute_script zfs/create-filesystem.sh
execute_script zfs/configure-pacman-mirrors.sh
execute_script zfs/create-zfs-pool.sh
execute_script zfs/create-zfs-datasets.sh
execute_script zfs/export-import-zfs-pool.sh
execute_script zfs/configure-root-filesystem.sh
execute_script zfs/configure-fstab-for-zfs.sh
execute_script zfs/pacstrap-for-zfs.sh
execute_script zfs/configure-pacman.sh

cat <<-'EOF'

Nästa steg:

arch-chroot /mnt
curl -s $URL/setup-system-with-zfs-part-2.sh | bash
EOF
