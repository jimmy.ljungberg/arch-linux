#!/usr/bin/env bash
set -eu

execute_script () {
    curl -s $URL/$BRANCH/install/$1 | bash
}

pacman --sync --noconfirm \
base-devel \
bash-completion \
dkms \
git \
jq \
man-db \
man-pages \
pacman-contrib \
reflector \
rsync \
sudo \
texinfo \
tmux \
tree \
unzip \
wget \
zip

execute_script configure-locale.sh
execute_script configure-network.sh
execute_script mkinitcpio.sh
execute_script zfs/configure-grub.sh
execute_script configure-systemd.sh
execute_script add-sudo-user.sh
execute_script configure-console.sh

cd /tmp
rm -fr yay-git
sudo -u $USER_NAME git clone https://aur.archlinux.org/yay-git.git
cd yay-git
sudo -u $USER_NAME makepkg --syncdeps --install --noconfirm
rm -fr yay-git
cd ..

cat << "EOF"

Sista steg:

Tilldela lösenord för $USER_NAME
Tilldela lösenord för root

Exekvera följande kommandon:
exit
umount /mnt/boot/efi
zfs umount -a
zpool export $POOL_NAME
reboot
EOF
