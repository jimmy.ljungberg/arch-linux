#!/usr/bin/env bash
set -eu

zfs rollback zroot/ROOT/default@$1
zfs rollback -r zroot/var@$1

echo "Starta om datorn"
