# Installera Arch Linux
Jag har ett antal scripts för att underlätta installation och konfiguration enligt mina önskemål. 

Scripten nås via URL
```bash
https://gitlab.com/jimmy.ljungberg/arch-linux/-/raw/main/<script>
```

För att exekvera script använd `curl -s <url>/<script> | bash`

Vill man se vad ett script gör går det förstås bra att bara utelämna pipningen till `bash` eller dirigera till en fil och därmed också möjligt redigera scriptet.

En del script kräver en eller flera parametrar. Saknas dessa parametrar visas ett meddelande om "VARIABEL: unbound variable". Tilldela variabeln ett värde i samband med pipning till bash.

Exempel:

Vid exekvering av script `wipe-disk.sh`:

```bash
curl -s https://gitlab.com/jimmy.ljungberg/arch-linux/-/raw/main/wipe-disk.sh | bash
```

visas meddelande `DISK: unbound variable`. Vill jag partitionera `/dev/nvme0n1` som boot-disk exekverar jag scriptet enligt följande:

```bash
curl -s https://gitlab.com/jimmy.ljungberg/arch-linux/-/raw/main/wipe-disk.sh | DISK=/dev/nvme0n1 bash
```

## Installation

### Förberedelser
1. Skapa [bootbar USB-sticka](https://wiki.archlinux.org/title/Installation_guide#Pre-installation)
1. Boota upp från installationsmedium
1. Byt till svenskt tangentbord:
   ```bash
   loadkeys sv-latin1
   ```

#### LVM
Tilldela variabler som scripten använder:

* `export URL=https://gitlab.com/jimmy.ljungberg/arch-linux/-/raw`
* `export BRANCH=main`
* `export BOOT_DEVICE` - T.ex. /dev/sda eller /dev/nvme0n1
* `export VOLUME_GROUP` - T.ex. system, lvm eller vg
* `export HOST_NAME` - Namn på datorn, t.ex. arch-linux, dator
* `export USER_NAME` - Namn på användare som skapas med sudo-rättigheter

Efter ovanstående variabler tilldelats kan du exekvera:

```bash
curl -s $URL/$BRANCH/setup-with-lvm.sh | bash
```

#### ZFS
1. Aktivera ZFS:  
  *2024-02-24: Scriptet finns fortfarande, men det rapporterar "No ZFS modul found" och jag har varken kommandot zpool eller zfs efter scriptet exekverats.*
   ```bash
   curl -s https://raw.githubusercontent.com/eoli3n/archiso-zfs/master/init | bash
   ```  
   *Meddelandet `error: command failed to execute correctly` brukar visas.*  
   *Efter meddelandet **ZFS is ready** visas har du access till kommandona `zpool` och `zfs`* 

Tilldela variabler som scripten använder:

* `export URL=https://gitlab.com/jimmy.ljungberg/arch-linux/-/raw/main`
* `export DISK` - Sökväg till device under `/dev`
* `export DISK_BY_ID` - Sökväg till device under `/dev/disk/by-id`
* `export HOST_NAME` - Namn på datorn
* `export POOL_NAME` - Namn på zfs pool, `zroot` används i dokumentation
* `export USER_NAME` - Namn på användare som skapas med sudo-rättigheter

Efter ovanstående variabler tilldelats kan du exekvera:

```bash
curl -s $URL/$BRANCH/setup-system-with-zfs.sh | bash
```

När det scriptet är klart gör man en `arch-chroot` (Script visar det exakta kommandot i slutet) och fortsätter installationen därifrån.

Totalt är det två scripts som du exekverar för att få ett system som bootar Arch Linux. Det är ett minimalt system utan GUI.
