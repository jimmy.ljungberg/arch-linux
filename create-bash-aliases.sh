#!/usr/bin/env bash

cat <<-'EOF' >> $HOME/.bash_aliases
alias ls='ls --color=auto'
alias la='ls -a'
alias ll='ls -l'
alias vi='vim'
EOF
