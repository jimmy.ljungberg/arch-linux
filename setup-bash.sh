#!/usr/bin/env bash
set -eu

if [ ! -d $HOME/.bashrc.d ]; then
    mkdir $HOME/.bashrc.d
fi
