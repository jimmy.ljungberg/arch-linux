#!/usr/bin/env bash
set -eu

execute_script () {
    curl -s $URL/$BRANCH/install/$1 | bash
}

execute_script install-console-packages.sh
execute_script configure-locale.sh
execute_script configure-network.sh
execute_script lvm/mkinitcpio.sh
execute_script lvm/systemd-boot.sh
execute_script configure-console.sh

execute_script add-sudo-user.sh
# OBS: förutsätter att add-sudo-user.sh redan exekverats
execute_script install-yay.sh

cat <<-"EOF"

Sista steg:

Tilldela lösenord för $USER_NAME
Tilldela lösenord för root

Exekvera följande kommandon:
exit
umount -R /mnt
reboot
EOF
