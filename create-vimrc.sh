#!/usr/bin/env bash
set -eux

cat <<-'EOF' > $HOME/.vimrc
set background=dark
set laststatus=2
set number
set relativenumber
set shiftwidth=4
set tabstop=4
set t_co=256

colorscheme solarized

syntax enabled

" Powerline configuration
python3 from powerline.vim import setup as powerline_setup
python3 powerline_setup()
python3 del powerline_setup
EOF
