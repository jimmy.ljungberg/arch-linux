#!/usr/bin/env bash
set -eu

pacman --sync --noconfirm \
firefox \
gnome \
gnome-shell-extension-appindicator \
gnome-tweaks \
solaar \
xorg-mkfontscale \
webp-pixbuf-loader

gsettings set org.gnome.desktop.calendar show-weekdate true

if $(yay --version); then
    pacman --sync --noconfirm libqtxdg
    yay --sync --noconfirm enpass-bin
fi

systemctl enable gdm.service
