#!/usr/bin/env bash
set -eu

# Installerad sedan tidigare
#yes | sudo pacman -S iptables-nft

sudo pacman -S --noconfirm \
dmidecode \
dnsmasq \
edk2-ovmf \
libvirt \
qemu \
swtpm \
virt-manager \
virt-viewer

# Förväntar sig dnsmasq. Mitt problem är att få dnsmasq att fungera bra tillsammans med LXD.
# Se https://gitlab.com/jimmy.ljungberg/arch-linux/-/issues/4
sudo systemctl enable --now libvirtd.service
