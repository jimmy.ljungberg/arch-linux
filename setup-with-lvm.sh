#!/usr/bin/env bash
set -eu

execute_script () {
    curl -s $URL/$BRANCH/install/$1 | bash
}

timedatectl set-timezone Europe/Stockholm

execute_script lvm/unmount-under-mnt.sh
execute_script lvm/delete-configured-lvm.sh
execute_script wipe-all-disks.sh
execute_script lvm/partition-disks.sh
execute_script lvm/setup-lvm.sh
execute_script lvm/create-filesystems.sh
execute_script lvm/mount-under-mnt.sh
execute_script lvm/setup-minimal-system.sh
execute_script lvm/configure-fstab.sh

cat <<-'EOF'

Nästa steg:

arch-chroot /mnt
curl -s $URL/$BRANCH/setup-with-lvm-2.sh | bash
EOF
